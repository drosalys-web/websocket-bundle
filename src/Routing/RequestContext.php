<?php

/*
 * This file is part of the websocket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\Routing;

use Symfony\Component\Routing\RequestContext as BaseContext;

/**
 * Class RequestContext
 *
 * @author Benjamin Georgeault
 */
class RequestContext extends BaseContext
{
    public function __construct(string $baseUrl = '', string $host = 'localhost', bool $ssl = false, int $httpPort = 80, int $httpsPort = 443)
    {
        parent::__construct(
            $baseUrl ? '/' . trim($baseUrl, '/') : '',
            'GET',
            $host ?? 'localhost',
            $ssl ? 'https' : 'http',
            $httpPort,
            $httpsPort
        );
    }
}
