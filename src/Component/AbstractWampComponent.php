<?php

/*
 * This file is part of the websocket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\Component;

use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;

/**
 * Class AbstractWampComponent
 *
 * @author Benjamin Georgeault
 */
abstract class AbstractWampComponent extends AbstractComponent implements WampServerInterface
{
    /**
     * @inheritDoc
     */
    public function onCall(ConnectionInterface $conn, $id, $topic, array $params)
    {
    }

    /**
     * @inheritDoc
     */
    public function onSubscribe(ConnectionInterface $conn, $topic)
    {
    }

    /**
     * @inheritDoc
     */
    public function onUnSubscribe(ConnectionInterface $conn, $topic)
    {
    }

    /**
     * @inheritDoc
     */
    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible)
    {
    }
}
