<?php

/*
 * This file is part of the web-socket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\Routing\Loader;

use Ratchet\ComponentInterface;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

use Symfony\Component\Routing\Loader\AnnotationClassLoader as BaseLoader;

/**
 * Class AnnotatedRouteControllerLoader
 *
 * @author Benjamin Georgeault
 */
class AnnotatedRouteControllerLoader extends BaseLoader
{
    /**
     * @var string
     */
    protected $routeAnnotationClass = 'DrosalysWeb\\Bundle\\WebSocketBundle\\Routing\\Annotation\\Route';

    public function load($class, $type = null): RouteCollection
    {
        if (!class_exists($class)) {
            throw new \InvalidArgumentException(sprintf('Class "%s" does not exist.', $class));
        }

        $class = new \ReflectionClass($class);
        if ($class->isAbstract()) {
            throw new \InvalidArgumentException(sprintf(
                'Annotations from class "%s" cannot be read as it is abstract.',
                $class->getName()
            ));
        }

        if (!$class->implementsInterface(ComponentInterface::class)) {
            throw new \InvalidArgumentException(sprintf(
                'The class "%s" has to implement "%s" to be annotated with "%s".',
                $class->getName(),
                ComponentInterface::class,
                $this->routeAnnotationClass
            ));
        }

        $collection = new RouteCollection();
        $collection->addResource(new FileResource($class->getFileName()));

        foreach ($this->reader->getClassAnnotations($class) as $annot) {
            if ($annot instanceof $this->routeAnnotationClass) {
                $collection->add(
                    $annot->getName() ?? $this->generateDefaultRouteName($class),
                    $this->createRouteInstance($annot, $class)
                );
            }
        }

        return $collection;
    }

    /**
     * @param $annot
     * @param \ReflectionClass $class
     * @return Route
     */
    protected function createRouteInstance($annot, \ReflectionClass $class): Route
    {
        return (new Route($annot->getPath(), $annot->getDefaults(), $annot->getRequirements(), $annot->getOptions()))
            ->setDefault('_controller', $class->getName())
        ;
    }

    /**
     * @param \ReflectionClass $class
     * @return string
     */
    protected function generateDefaultRouteName(\ReflectionClass $class): string
    {
        $name = str_replace('\\', '_', $class->name);
        $name = \function_exists('mb_strtolower') && preg_match('//u', $name) ? mb_strtolower($name, 'UTF-8') : strtolower($name);

        return preg_replace([
            '/(bundle|controller)_/',
            '/action(_\d+)?$/',
            '/__/',
        ], [
            '_',
            '\\1',
            '_',
        ], $name);
    }

    protected function configureRoute(Route $route, \ReflectionClass $class, \ReflectionMethod $method, $annot)
    {
    }
}
