<?php

/*
 * This file is part of the websocket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\Monitoring;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

/**
 * Class EmailMonitoring
 *
 * @author Benjamin Georgeault
 */
class EmailMonitoring implements MonitoringInterface
{
    private MailerInterface $mailer;
    /**
     * @var string[]
     */
    private array $to;
    private string $subject;
    private string $from;

    /**
     * @param string[] $to
     */
    public function __construct(MailerInterface $mailer, array $to, string $subject, string $from)
    {
        $this->mailer = $mailer;
        $this->to = $to;
        $this->subject = $subject;
        $this->from = $from;
    }

    public function catchException(\Throwable $e, \DateTimeInterface $throwAt): void
    {
        ($email = new Email())
            ->to(...$this->to)
            ->subject($this->subject)
            ->text(sprintf(
                'Exception catch at %s.'."\n\n".
                '%s'."\n",
                $throwAt->format('YYYY-MM-dd hh:ii:ss'),
                $this->generateMessage($e)
            ))
        ;

        if (null !== $this->from) {
            $email->from($this->from);
        }

        $this->mailer->send($email);
    }

    private function generateMessage(\Throwable $e): string
    {
        return sprintf(
            '--------------'.
            'Message: "%s"'."\n".
            'Code: "%s"'."\n".
            'Trace:'."\n".
            '%s'."\n".
            '--------------',
            $e->getMessage(),
            $e->getCode(),
            $e->getTraceAsString()
        );
    }
}
