<?php

/*
 * This file is part of the web-socket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\Routing\Loader;

use Symfony\Component\Routing\Loader\XmlFileLoader as BaseLoader;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class XmlFileLoader
 *
 * @author Benjamin Georgeault
 */
class XmlFileLoader extends BaseLoader
{
    /**
     * @var string[]
     */
    private static array $disabledNames = [
        'condition',
    ];

    /**
     * @var string[]
     */
    private static array $disabledAttributes = [
        'host', 'schemes', 'methods',
    ];

    /**
     * @inheritDoc
     */
    protected function parseRoute(RouteCollection $collection, \DOMElement $node, $path)
    {
        foreach (self::$disabledAttributes as $disabledAttribute) {
            if ($node->hasAttribute($disabledAttribute)) {
                throw new \InvalidArgumentException(sprintf(
                    'Disabled tag\'s attribute "%s" used in file "%s". Disabled ones are: "%s".',
                    $disabledAttribute,
                    $path,
                    implode('", "', self::$disabledAttributes)
                ));
            }
        }

        foreach ($node->getElementsByTagNameNS(self::NAMESPACE_URI, '*') as $n) {
            if ($node !== $n->parentNode) {
                continue;
            }

            if (in_array($n->localName, self::$disabledNames)) {
                throw new \InvalidArgumentException(sprintf(
                    'Disabled tag "%s" used in file "%s". Disabled ones are: "%s".',
                    $n->localName,
                    $path,
                    implode('", "', self::$disabledNames)
                ));
            }
        }

        parent::parseRoute($collection, $node, $path);
    }
}
