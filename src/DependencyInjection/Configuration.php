<?php

/*
 * This file is part of the web-socket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class Configuration
 *
 * @author Benjamin Georgeault
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @inheritdoc
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('drosalys_web_socket');

        $treeBuilder->getRootNode()
            ->children()
                ->append($this->serverNode())
                ->append($this->routingNode())
                ->append($this->monitoringNode())
                ->append($this->sessionNode())
                ->append($this->frontNode())
            ->end()
        ;

        return $treeBuilder;
    }

    /**
     * @return NodeDefinition
     */
    private function serverNode(): NodeDefinition
    {
        return (new TreeBuilder('server'))->getRootNode()
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('address')
                    ->defaultValue('127.0.0.1')
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('port')
                    ->defaultValue('8080')
                    ->cannotBeEmpty()
                ->end()
                ->arrayNode('context')
                    ->ignoreExtraKeys(false)
                ->end()
            ->end()
        ;
    }

    /**
     * @return NodeDefinition
     */
    private function routingNode(): NodeDefinition
    {
        return (new TreeBuilder('routing'))->getRootNode()
            ->addDefaultsIfNotSet()
            ->children()
                ->booleanNode('annotation')
                    ->defaultFalse()
                ->end()
                ->scalarNode('resource')
                    ->defaultValue('%kernel.project_dir%/config/ws_routes.yaml')
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('user_provider')
                    ->defaultValue(UserProviderInterface::class)
                    ->cannotBeEmpty()
                ->end()
            ->end()
        ;
    }

    /**
     * @return NodeDefinition
     */
    private function monitoringNode(): NodeDefinition
    {
        return (new TreeBuilder('monitoring'))->getRootNode()
            ->canBeEnabled()
            ->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('email')
                    ->canBeDisabled()
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('from')
                            ->cannotBeEmpty()
                        ->end()
                        ->scalarNode('subject')
                            ->cannotBeEmpty()
                            ->defaultValue('Web Socket monitoring')
                        ->end()
                        ->arrayNode('to')
                            ->beforeNormalization()->castToArray()->end()
                            ->cannotBeEmpty()
                            ->scalarPrototype()
                                ->cannotBeEmpty()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    /**
     * @return NodeDefinition
     */
    private function sessionNode(): NodeDefinition
    {
        return (new TreeBuilder('session'))->getRootNode()
            ->canBeEnabled()
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('handler')
                    ->cannotBeEmpty()
                ->end()
                ->arrayNode('firewalls')
                    ->beforeNormalization()->castToArray()->end()
                    ->cannotBeEmpty()
                    ->scalarPrototype()
                        ->cannotBeEmpty()
                    ->end()
                ->end()
            ->end()
        ;
    }

    /**
     * @return NodeDefinition
     */
    private function frontNode(): NodeDefinition
    {
        return (new TreeBuilder('front'))->getRootNode()
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('host')
                    ->defaultValue('localhost')
                ->end()
                ->booleanNode('ssl')
                    ->defaultFalse()
                ->end()
                ->scalarNode('base_url')
                    ->defaultNull()
                ->end()
                ->integerNode('port')
                    ->defaultValue(8080)
                ->end()
                ->integerNode('ssl_port')
                    ->defaultValue(8080)
                ->end()
            ->end()
        ;
    }
}
