<?php

/*
 * This file is part of the web-socket-bundle package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\DependencyInjection;

use DrosalysWeb\Bundle\WebSocketBundle\Monitoring\EmailMonitoring;
use DrosalysWeb\Bundle\WebSocketBundle\Monitoring\MonitoringRegister;
use DrosalysWeb\Bundle\WebSocketBundle\Routing\Loader\AnnotatedRouteControllerLoader;
use Ratchet\Session\SessionProvider;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Loader\AnnotationDirectoryLoader;
use Symfony\Component\Routing\Loader\AnnotationFileLoader;

/**
 * Class DrosalysWebWebSocketExtension
 *
 * @author Benjamin Georgeault
 */
class DrosalysWebWebSocketExtension extends Extension
{
    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('console.yaml');
        $loader->load('server.yaml');
        $loader->load('routing.yaml');

        if (class_exists('Twig\\Extension\\AbstractExtension')) {
            $loader->load('twig.yaml');
        }

        $container->setParameter(
            'drosalys.web_socket.server_uri',
            $config['server']['address'].':'.$config['server']['port']
        );

        $context = [];
        if (array_key_exists('context', $config['server'])) {
            $context = $config['server']['context'];
        }
        $container->setParameter('drosalys.web_socket.server_context', $context);

        $container->setParameter('drosalys.web_socket.router_resource', $config['routing']['resource']);

        $this->registerAnnotationServices($config, $container);
        $this->registerMonitoringServices($config, $container);
        $this->registerSessionServices($config, $container);
        $this->applyFrontRequestContext($config, $container);
        $this->registerRoutingServices($config, $container);
    }

    /**
     * @inheritDoc
     */
    public function getAlias()
    {
        return 'drosalys_web_socket';
    }

    private function registerRoutingServices(array $config, ContainerBuilder $container)
    {
        $definition = $container->getDefinition('drosalys.web_socket.routing.router')
            ->setArgument(3, new Reference($config['routing']['user_provider']))
        ;
    }

    private function registerAnnotationServices(array $config, ContainerBuilder $container)
    {
        if (true !== $config['routing']['annotation']) {
            return;
        }

        $container->register('drosalys.web_socket.routing.loader.annotation', AnnotatedRouteControllerLoader::class)
            ->setPublic(false)
            ->addTag('drosalys.ws.routing.loader', ['priority' => -10])
            ->addArgument(new Reference('annotation_reader'))
        ;

        $container->register('drosalys.web_socket.routing.loader.annotation.directory', AnnotationDirectoryLoader::class)
            ->setPublic(false)
            ->addTag('drosalys.ws.routing.loader', ['priority' => -10])
            ->setArguments([
                new Reference('file_locator'),
                new Reference('drosalys.web_socket.routing.loader.annotation'),
            ])
        ;

        $container->register('drosalys.web_socket.routing.loader.annotation.file', AnnotationFileLoader::class)
            ->setPublic(false)
            ->addTag('drosalys.ws.routing.loader', ['priority' => -10])
            ->setArguments([
                new Reference('file_locator'),
                new Reference('drosalys.web_socket.routing.loader.annotation'),
            ])
        ;
    }

    private function registerMonitoringServices(array $config, ContainerBuilder $container)
    {
        if (!$config['monitoring']['enabled']) {
            return;
        }

        // Monitoring register
        $container->setAlias(MonitoringRegister::class, 'drosalys.web_socket.monitoring.register');
        $container->register('drosalys.web_socket.monitoring.register', MonitoringRegister::class)
            ->setPublic(false)
        ;

        // Monitoring
        if ($config['monitoring']['email']['enabled']) {
            if (!interface_exists('Symfony\\Component\\Mailer\\MailerInterface')) {
                throw new LogicException('Monitoring by email support cannot be enabled as the Symfony\'s Mailer component is not installed. Try running "composer require symfony/mailer".');
            }

            $container->setAlias(EmailMonitoring::class, 'drosalys.web_socket.monitoring.email');
            $container->register('drosalys.web_socket.monitoring.email', EmailMonitoring::class)
                ->setPublic(false)
                ->addTag('drosalys.ws.monitoring')
                ->setArguments([
                    new Reference(MailerInterface::class),
                    $config['monitoring']['email']['to'],
                    $config['monitoring']['email']['subject'],
                    $config['monitoring']['email']['from'],
                ])
            ;
        }
    }

    private function registerSessionServices(array $config, ContainerBuilder $container)
    {
        if (!$config['session']['enabled']) {
            $container->setAlias('drosalys.web_socket.wrapped_http_server', 'drosalys.web_socket.routing.router');
            return;
        }

        $container->setAlias('drosalys.web_socket.wrapped_http_server', 'drosalys.web_socket.session.provider');
        $container->register('drosalys.web_socket.session.provider', SessionProvider::class)
            ->setPublic(false)
            ->setArguments([
                new Reference('drosalys.web_socket.routing.router'),
                new Reference($config['session']['handler']),
            ])
        ;

        $container->getDefinition('drosalys.web_socket.routing.router')
            ->setArgument(4, $config['session']['firewalls'])
        ;
    }

    private function applyFrontRequestContext(array $config, ContainerBuilder $container)
    {
        $container->getDefinition('drosalys.web_socket.routing.request_context')
            ->setArguments([
                $config['front']['base_url'],
                $config['front']['host'],
                $config['front']['ssl'],
                $config['front']['port'],
                $config['front']['ssl_port']
            ])
        ;
    }
}
