<?php

/*
 * This file is part of the websocket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\Server;

use Psr\Log\LoggerAwareTrait;
use Ratchet\Server\IoServer as BaseServer;
use React\Socket\ConnectionInterface;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class IoServer
 *
 * @author Benjamin Georgeault
 */
class IoServer extends BaseServer
{
    use LoggerAwareTrait;

    private ?OutputInterface $output = null;
    private ?Application $application = null;

    public function setOutput(?OutputInterface $output): self
    {
        $this->output = $output;
        return $this;
    }

    public function setApplication(?Application $application): self
    {
        $this->application = $application;
        return $this;
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function handleConnect($conn)
    {
        if (null !== $this->output && $this->output->isVerbose()) {
            $this->output->writeln(sprintf(
                'New connection with resource ID <info>%s</info> from <info>%s</info>.',
                (int) $conn->stream,
                $conn->getRemoteAddress()
            ));
        }

        if (null !== $this->logger) {
            $this->logger->info(sprintf(
                'New connection with resource ID "%s" from "%s".',
                (int) $conn->stream,
                $conn->getRemoteAddress()
            ));
        }

        parent::handleConnect($conn);
    }

    /**
     * @param string $data
     * @param ConnectionInterface $conn
     */
    public function handleData($data, $conn)
    {
        if (null !== $this->output && $this->output->isVerbose()) {
            $this->output->writeln(sprintf(
                'Receive data from resource ID <info>%s</info> (<info>%s</info>).',
                (int) $conn->stream,
                $conn->getRemoteAddress()
            ));

            if ($this->output->isVeryVerbose()) {
                $this->output->writeln([
                    '<comment>--------</comment>',
                    OutputFormatter::escape($data),
                    '<comment>--------</comment>',
                ]);
            }
        }

        if (null !== $this->logger) {
            $this->logger->info(sprintf(
                'Receive data from resource ID "%s" ("%s").',
                (int) $conn->stream,
                $conn->getRemoteAddress()
            ));
        }

        parent::handleData($data, $conn);
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function handleEnd($conn)
    {
        if (null !== $this->output && $this->output->isVerbose()) {
            $this->output->writeln(sprintf(
                'Close connection for resource ID <info>%s</info> (<info>%s</info>).',
                (int) $conn->stream,
                $conn->getRemoteAddress()
            ));
        }

        if (null !== $this->logger) {
            $this->logger->info(sprintf(
                'Close connection for resource ID "%s" ("%s").',
                (int) $conn->stream,
                $conn->getRemoteAddress()
            ));
        }

        parent::handleEnd($conn);
    }

    /**
     * @param \Exception $e
     * @param ConnectionInterface $conn
     */
    public function handleError(\Exception $e, $conn)
    {
        if (null !== $this->output) {
            $this->output->writeln(sprintf(
                'Error for resource ID <info>%s</info> (<info>%s</info>): <error>%s</error>',
                (int) $conn->stream,
                $conn->getRemoteAddress(),
                $e->getMessage()
            ));

            if (null !== $this->application) {
                $this->application->renderThrowable($e, $this->output);
            } else {
                if ($this->output->isVerbose()) {
                    $this->output->writeln([
                        '<error>--------</error>',
                        sprintf('Message: %s', OutputFormatter::escape($e->getMessage())),
                        sprintf('Code: %s', $e->getCode()),
                        sprintf('File: %s', $e->getFile()),
                        sprintf('Line: %d', $e->getLine()),
                        OutputFormatter::escape($e->getTraceAsString()),
                        '<error>--------</error>',
                    ]);
                }

                if ($this->output->isVeryVerbose()) {
                    $this->output->writeln([
                        OutputFormatter::escape($e->getTraceAsString()),
                        '<error>--------</error>',
                    ]);
                }
            }
        }

        if (null !== $this->logger) {
            $this->logger->error(sprintf(
                'Error for resource ID "%s" ("%s"): "%s" in file "%s" at line %d.',
                (int) $conn->stream,
                $conn->getRemoteAddress(),
                $e->getMessage(),
                $e->getFile(),
                $e->getLine()
            ));
            $this->logger->debug($e->getTraceAsString());
        }

        parent::handleError($e, $conn);
    }
}
